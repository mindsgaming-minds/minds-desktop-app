# A Simple Desktop App For Linux based Systems

## Install 

<code>
git clone https://gitlab.com/mindsgaming-minds/minds-desktop-app.git
</code>

<code>
cd minds-desktop-app
</code>

<code>
/$HOME/minds-desktop-app/Minds</code>

## Other Install Options:
[MDA-Install](https://minds-desktop-app.glitch.me/install.html)